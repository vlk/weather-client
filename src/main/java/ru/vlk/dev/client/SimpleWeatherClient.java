package ru.vlk.dev.client;

import ru.vlk.dev.model.RootBean;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class SimpleWeatherClient implements WeatherClient {

    private Client client;
    private String url;

    public SimpleWeatherClient() {
        url = "http://api.openweathermap.org/data/2.5/weather";
        client = ClientBuilder.newClient();
    }

    public List<RootBean> getAllWeather() {
        RootBean[] rootBeans = new RootBean[65536];
        int index = 0;
        for (int longitude = 0; longitude <= 180; longitude++) {
            for (int latitude = 0; latitude <= 180; latitude++) {
                RootBean rootBean = getWeather(longitude, latitude);
                if (rootBean.getMain() != null) {
                    System.out.println("Lat:" + latitude + " Lon:" + longitude + " Temp:" + rootBean
                            .getMain().getTemp() + " Name:" + rootBean.getName());
                    rootBeans[index++] = rootBean;
                }
            }
        }
        return null;
    }

    public RootBean getWeather(int longitude, int latitude) {
        return client.target(url).queryParam("lon", longitude + "").queryParam("lat", latitude + "")
                .queryParam("appid", "44db6a862fba0b067b1930da0d769e98")
                .request(MediaType.APPLICATION_JSON_TYPE).get(RootBean.class);
    }

}
