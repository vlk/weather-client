package ru.vlk.dev.client;

import ru.vlk.dev.model.RootBean;

import java.util.List;

public interface WeatherClient {

    List<RootBean> getAllWeather();

    RootBean getWeather(int longitude, int latitude);
}
