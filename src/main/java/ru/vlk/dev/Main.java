package ru.vlk.dev;

import ru.vlk.dev.client.SimpleWeatherClient;
import ru.vlk.dev.client.WeatherClient;
import ru.vlk.dev.model.RootBean;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        WeatherClient client = new SimpleWeatherClient();
        List<RootBean> weatherList = client.getAllWeather();
    }
}
